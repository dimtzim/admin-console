import React from 'react';

const User = (props) => {
    return (  
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">{props.username}</h5>
                <button class="btn btn-primary" onClick={props.delete}>Delete</button>
            </div>
        </div>   
    )
}

export default User;
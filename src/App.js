import React, { Component } from 'react';
import './App.css';
import User from './Users/Users';

class App extends Component {
  state = {
    users: []
  }

  componentDidMount() {
    fetch('http://localhost:8080/users')
    .then(res => res.json())
    .then((data) => {
      this.setState({ users: data })
    })
    .catch(console.log)
  };

  deleteUserHandler = (userIndex) => {

    const requestOptions = {
      method: 'DELETE'
    };
  
    fetch("http://localhost:8080/delete/user/" + userIndex, requestOptions)
    .then(res => res.json())
    .then((result) => {
      this.setState({users: result})
    })
    .catch(console.log);
  }

  render() {

    return (
      <div className="App">
        <div>
            <center><h1>User List</h1></center>
            {this.state.users.map((user, index) => {
              return <User 
              username={user.username}
              delete={() => this.deleteUserHandler(user.username)}
              />
            })}
          </div>
      </div>
    );
  };
}

export default App;
